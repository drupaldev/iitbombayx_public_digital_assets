This folder contains digital assets, which will get automatically loaded amongst other drupal directories during setup.
All these files are meant to serve as row content for default frontend webpages.

**Folder contains:**

- Logo Image    : It will get displayed on Home Page and footer.

- Favicon Image : These will get displayed on browser tab.

- Banner images : They will get displayed on home page slider. 
                  Note: Three image size each for Desktop, Tablet and Mobile

- Static page content like about page, contact us page etc. is also included within the directory. 

- config.yml file defines site content for asset mapping.


**Requirements:**

 1. Logo Image size should be of size (133 X 36)

 2. Favicon Image should be of size (32 X 32 )

 3. Banner Images Large should be of size (1170 X 359 )

 4. Banner Images Medium should be of size (950 X 359 )
 
 5. Banner Images Small should be of size (750 X 359 )
 
 6. Static Pages file should be html template.

 7. Hexadecimal value for theme color
 
 8. Edxpath: This should be your edx site domain
 t


**Note:** You can add more banners, just need to add them to config file.
      keep naming convention same as used in config file. 
      But while editing mappings do it with care.
      - Do NOT change keys in yaml file
      - Do NOT use tabs in yaml file
      - Beware of indentation

**Process to use**

1. Add your files to folder
2. Edit config.yml file with correct mappings

**Manual Installation**
1. Place this directory to drupals root/site/default/files
2. Go to drupal admin panel >> extend >> search module "import content" >> install 
